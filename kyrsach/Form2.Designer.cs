﻿
namespace kyrsach
{
   public partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lname = new System.Windows.Forms.TextBox();
            this.city = new System.Windows.Forms.TextBox();
            this.phnnumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.town = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.postindex = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.Add.Location = new System.Drawing.Point(30, 388);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(132, 50);
            this.Add.TabIndex = 0;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(663, 388);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(125, 50);
            this.Close.TabIndex = 1;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ім\'я";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Прізвище";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 239);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Email";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Номер телефону";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(350, 65);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(187, 26);
            this.textBox1.TabIndex = 6;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lname
            // 
            this.lname.Location = new System.Drawing.Point(350, 123);
            this.lname.Name = "lname";
            this.lname.Size = new System.Drawing.Size(187, 26);
            this.lname.TabIndex = 7;
            // 
            // city
            // 
            this.city.Location = new System.Drawing.Point(350, 236);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(187, 26);
            this.city.TabIndex = 8;
            // 
            // phnnumber
            // 
            this.phnnumber.Location = new System.Drawing.Point(350, 183);
            this.phnnumber.Name = "phnnumber";
            this.phnnumber.Size = new System.Drawing.Size(187, 26);
            this.phnnumber.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(81, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Місто";
            // 
            // town
            // 
            this.town.Location = new System.Drawing.Point(350, 286);
            this.town.Name = "town";
            this.town.Size = new System.Drawing.Size(187, 26);
            this.town.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(81, 339);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Поштовий індекс";
            // 
            // postindex
            // 
            this.postindex.Location = new System.Drawing.Point(350, 333);
            this.postindex.Name = "postindex";
            this.postindex.Size = new System.Drawing.Size(187, 26);
            this.postindex.TabIndex = 13;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::kyrsach.Properties.Resources._66b6541054ad3157f88dabcce021fe02;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.postindex);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.town);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.phnnumber);
            this.Controls.Add(this.city);
            this.Controls.Add(this.lname);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Add);
            this.Name = "Form2";
            this.Text = "Форма реєстрації";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox lname;
        private System.Windows.Forms.TextBox city;
        private System.Windows.Forms.TextBox phnnumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox town;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox postindex;
    }
}