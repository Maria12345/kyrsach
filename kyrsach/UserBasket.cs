﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace kyrsach
{
    public partial class UserBasket : Form
    {
        private SqlConnection sqlConnection = null;
        public List<ShopBasket> ShopBaskets;
        public UserBasket(List<ShopBasket> shopBaskets)
        {
            InitializeComponent();
            foreach (ShopBasket i in shopBaskets)
            {
                basketlist.Items.Add(i.productname + "  " + i.price + "  " + i.customername);
            }
            
        }

        private void UserBasket_Load(object sender, EventArgs e)
        {
            sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Base2"].ConnectionString);
            sqlConnection.Open();

            totalsum.Visible = false;
            
        }

        private void deleteItem_Click(object sender, EventArgs e)
        {

            ShopBaskets.RemoveAt(basketlist.SelectedIndex);
            string del = basketlist.SelectedItem.ToString();
            basketlist.Items.Remove(del);
            
        }

        private void bill_Click(object sender, EventArgs e)
        {
            double sum = 0;
            for (int i = 0; i < ShopBaskets.Count; i++)
            {
                
                sum +=Convert.ToDouble( ShopBaskets[i].price);
            }
            if (delivery.SelectedIndex==1)
            {
                sum += 100.0;
            }
            if (delivery.SelectedIndex == 2)
            {
                sum += 50.0;
            }
            if (delivery.SelectedIndex == 3)
            {
                sum += 60.0;
            }
            totalsum.Text = sum.ToString();
            totalsum.Visible = true;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void basketlist_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void makeanorder_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ShopBaskets.Count; i++)
            {
                SqlCommand sqlCommand1 = new SqlCommand($"INSERT INTO Orders (CustomerID,ProductID) SELECT CUSTOMERs.ID, Products.ID FROM CUSTOMERs , " +
              $"Products WHERE  CUSTOMERs.LName LIKE N'{ShopBaskets[i].customername}'  AND Products.Name LIKE N'{ShopBaskets[i].productname}' ", sqlConnection);
                MessageBox.Show(sqlCommand1.ExecuteNonQuery().ToString());
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
