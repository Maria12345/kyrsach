﻿
namespace kyrsach
{
    partial class UserBasket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.basketlist = new System.Windows.Forms.ListBox();
            this.deleteItem = new System.Windows.Forms.Button();
            this.bill = new System.Windows.Forms.Button();
            this.totalsum = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.makeanorder = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.delivery = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // basketlist
            // 
            this.basketlist.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.basketlist.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.basketlist.FormattingEnabled = true;
            this.basketlist.ItemHeight = 20;
            this.basketlist.Location = new System.Drawing.Point(13, 13);
            this.basketlist.Name = "basketlist";
            this.basketlist.Size = new System.Drawing.Size(318, 424);
            this.basketlist.TabIndex = 0;
            this.basketlist.SelectedIndexChanged += new System.EventHandler(this.basketlist_SelectedIndexChanged);
            // 
            // deleteItem
            // 
            this.deleteItem.Location = new System.Drawing.Point(335, 13);
            this.deleteItem.Name = "deleteItem";
            this.deleteItem.Size = new System.Drawing.Size(112, 52);
            this.deleteItem.TabIndex = 1;
            this.deleteItem.Text = "Видалити з кошика";
            this.deleteItem.UseVisualStyleBackColor = true;
            this.deleteItem.Click += new System.EventHandler(this.deleteItem_Click);
            // 
            // bill
            // 
            this.bill.Location = new System.Drawing.Point(341, 371);
            this.bill.Name = "bill";
            this.bill.Size = new System.Drawing.Size(112, 52);
            this.bill.TabIndex = 2;
            this.bill.Text = "Сума до сплати ";
            this.bill.UseVisualStyleBackColor = true;
            this.bill.Click += new System.EventHandler(this.bill_Click);
            // 
            // totalsum
            // 
            this.totalsum.AutoSize = true;
            this.totalsum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.totalsum.Location = new System.Drawing.Point(459, 377);
            this.totalsum.Name = "totalsum";
            this.totalsum.Size = new System.Drawing.Size(93, 32);
            this.totalsum.TabIndex = 3;
            this.totalsum.Text = "label1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::kyrsach.Properties.Resources._0a8261b808db341d1fb54e1be5be1317;
            this.pictureBox1.Location = new System.Drawing.Point(524, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(277, 287);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // makeanorder
            // 
            this.makeanorder.Location = new System.Drawing.Point(689, 384);
            this.makeanorder.Name = "makeanorder";
            this.makeanorder.Size = new System.Drawing.Size(112, 55);
            this.makeanorder.TabIndex = 5;
            this.makeanorder.Text = "Створити замовлення";
            this.makeanorder.UseVisualStyleBackColor = true;
            this.makeanorder.Click += new System.EventHandler(this.makeanorder_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(337, 293);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Оберіть спосіб доставки";
            // 
            // delivery
            // 
            this.delivery.FormattingEnabled = true;
            this.delivery.Items.AddRange(new object[] {
            "Самовивіз з поштового відділення",
            "Доставка кур\'єром",
            "Нова пошта",
            "УкрПошта"});
            this.delivery.Location = new System.Drawing.Point(341, 328);
            this.delivery.Name = "delivery";
            this.delivery.Size = new System.Drawing.Size(121, 28);
            this.delivery.TabIndex = 11;
            // 
            // UserBasket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::kyrsach.Properties.Resources._66b6541054ad3157f88dabcce021fe02;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.delivery);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.makeanorder);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.totalsum);
            this.Controls.Add(this.bill);
            this.Controls.Add(this.deleteItem);
            this.Controls.Add(this.basketlist);
            this.Name = "UserBasket";
            this.Text = "Ваш кошик";
            this.Load += new System.EventHandler(this.UserBasket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button deleteItem;
        private System.Windows.Forms.Button bill;
        private System.Windows.Forms.Label totalsum;
        protected internal System.Windows.Forms.ListBox basketlist;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button makeanorder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox delivery;
    }
}